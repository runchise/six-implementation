# Case
Write six functions for an implementation of a stack: pop(), push(), top(), empty(), max()
and min() using either an Array or a Linked List. Please consider the time complexity of
your solution.

# How to run
1. compile `javac app.java`
2. run `java app`