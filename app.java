import java.util.Arrays;

public class app {
    public static void main(String[] args) {
        int[] randomNumbers = {233, 158, 925, 518, 363};
        System.out.println("Initial: " + Arrays.toString(randomNumbers));

        // pop
        randomNumbers = pop(randomNumbers);
        System.out.println("After pop: " + Arrays.toString(randomNumbers));

        // push
        randomNumbers = push(randomNumbers, 924);
        System.out.println("After push: " + Arrays.toString(randomNumbers));

        // top
        top(randomNumbers);

        // empty
        empty(randomNumbers);

        // max
        max(randomNumbers);

        // min
        min(randomNumbers);
    }

    /**
     * pop last element on stack
     * time-complexity: O(n)
     */
    private static int[] pop(int[] randomNumbers) {
        int newLength = randomNumbers.length - 1;
        int[] newList = new int[newLength];
        for (int i = 0; i < newLength; i++) {
            newList[i] = randomNumbers[i];
        }

        return newList;
    }

    /**
     * push new element on top of stack
     * time-complexity: O(n)
     */
    private static int[] push(int[] randomNumbers, int newNumber) {
        int newLength = randomNumbers.length + 1;
        int[] newList = new int[newLength];
        for (int i = 0; i < newLength; i++) {
            try {
                newList[i] = randomNumbers[i];
            } catch (ArrayIndexOutOfBoundsException e) {
                newList[i] = newNumber;
            }
        }

        return newList;
    }

    /**
     * print top/last element on a stack
     * time-complexity: O(1)
     */
    private static void top(int[] randomNumbers) {
        int lastNumber = randomNumbers[randomNumbers.length - 1];

        System.out.println("Top element: " + lastNumber);
    }

    /**
     * check if stack empty or not
     * time-complexity: O(1)
     */
    private static void empty(int[] randomNumbers) {
        System.out.println("Is empty: " + (randomNumbers.length == 0));
    }

    /**
     * show maximum number in stack
     * time-complexity: O(n)
     */
    private static void max(int[] randomNumbers) {
        int maxNumber = 0;
        for (int i = 0; i < randomNumbers.length; i++) {
            if (maxNumber < randomNumbers[i]) {
                maxNumber = randomNumbers[i];
            }
        }

        System.out.println("Max element: " + maxNumber);
    }

    /**
     * show minimal number in stack
     * time-complexity: O(n)
     */
    private static void min(int[] randomNumbers) {
        int minNumber = randomNumbers[randomNumbers.length-1];
        for (int i = 0; i < randomNumbers.length; i++) {
            if (minNumber > randomNumbers[i]) {
                minNumber = randomNumbers[i];
            }
        }

        System.out.println("Min element: " + minNumber);
    }
}
